# Copyright 2021-Coopdevs Treball SCCL (<https://coopdevs.org>)
# - Dani Quilez - <dani.quilez@coopdevs.org>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Base Rest External ID",
    "version": "12.0.1.0.0",
    "depends": ["base_rest"],
    "author": "Coopdevs Treball SCCL",
    "website": "https://gitlab.com/coopdevs/odoo-addons",
    "license": "AGPL-3",
    "summary": """
        Provide external ID field to unify external comunication with different systems.
    """,
    "data": [],
    "installable": True,
}
